# isFragmentPathDifferent

Tiny script to check if the path of the given SystemD fragment matches the first argument. This script was last modified in early 2018 and is no longer maintained.
